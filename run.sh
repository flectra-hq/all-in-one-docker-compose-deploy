#!/bin/bash

# Default values
reverse_proxy="N"
reverse_proxy_url=""
project_location="$HOME"  # Default location is the current user's home folder

slugify() {
    echo "$1" | sed 's/[^[:alnum:]]/-/g' | tr -s '-' | tr A-Z a-z
}

# Function to install Docker on Debian/Ubuntu based systems
install_docker_debian() {
    sudo apt update
    sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt update
    sudo apt install -y docker-ce docker-ce-cli containerd.io
}

# Function to install Docker on CentOS/RHEL based systems
install_docker_centos() {
    sudo yum install -y yum-utils device-mapper-persistent-data lvm2
    sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum install -y docker-ce docker-ce-cli containerd.io
}

# Function to install Docker Compose
install_docker_compose() {
    sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
}

# Function to generate a witty project name
generate_witty_project_name() {
    adjectives=("funny" "witty" "clever" "silly" "whimsical" "quirky" "amusing" "lively" "charming" "playful")
    nouns=("elephant" "banana" "octopus" "penguin" "robot" "ninja" "unicorn" "giraffe" "pirate" "dragon")
    adjective="${adjectives[$RANDOM % ${#adjectives[@]}]}"
    noun="${nouns[$RANDOM % ${#nouns[@]}]}"
    witty_project_name="$adjective$noun"
    echo "$witty_project_name"
}

# Function to print usage
print_usage() {
    echo "Usage: $0 -r <Reverse Proxy (Y/N)> -p <Reverse Proxy URL> -l <Project Location>"
    echo "Options:"
    echo "  -r, --reverse-proxy      Reverse Proxy (Y/N) [required] [default=N]"
    echo "  -p, --proxy-url          URL for Reverse Proxy if Reverse Proxy is enabled"
    echo "  -l, --project-location   Project Location [required]"
}

# Function to prompt for parameters if not provided
prompt_parameters() {
    echo "Please provide the following parameters:"
    read -rp "Reverse Proxy (Y/N) [default=N]: " reverse_proxy
    reverse_proxy=${reverse_proxy^^} # Convert to uppercase
    reverse_proxy=${reverse_proxy:-"N"}

    if [[ $reverse_proxy == "Y" ]]; then
        read -rp "URL for Reverse Proxy: " reverse_proxy_url

        # Check if URL structure contains "http://" or "https://"
        if [[ $reverse_proxy_url == http://* || $reverse_proxy_url == https://* ]]; then
            echo "Error: Reverse Proxy URL should not include 'http://' or 'https://'."
            exit 1
        fi

        # Check ports 80 and 443 for availability if reverse proxy is enabled
        if lsof -Pi :80 -sTCP:LISTEN -t >/dev/null; then
            echo "Port 80 is already in use."
            exit 1
        fi

        if lsof -Pi :443 -sTCP:LISTEN -t >/dev/null; then
            echo "Port 443 is already in use."
            exit 1
        fi
    fi

    read -rp "Project Location [default=$HOME]: " project_location
    project_location=${project_location:-"$HOME"}
}

# Function to check if Docker is installed
check_docker_installed() {
    if ! command -v docker &>/dev/null; then
        echo "Docker is not installed."
        return 1
    fi
    echo "Docker is already installed."
    return 0
}

# Function to check if Docker Compose is installed
check_docker_compose_installed() {
    if ! command -v docker-compose &>/dev/null; then
        echo "Docker Compose is not installed."
        return 1
    fi
    echo "Docker Compose is already installed."
    return 0
}

# Function to check if the project location exists and has proper write permissions
check_project_location() {
    if [[ ! -d "$project_location" ]]; then
        echo "Project location '$project_location' does not exist. Creating it..."
        mkdir -p "$project_location" || exit 1
    fi

    if [[ ! -w "$project_location" ]]; then
        echo "Insufficient permissions to write to project location '$project_location'."
        return 1
    fi

    echo "Project location '$project_location' is valid."
    return 0
}

# Function to check if ports are in use
check_ports() {
    local in_use=false

    # Check port 5432
    if lsof -Pi :5432 -sTCP:LISTEN -t >/dev/null; then
        echo "Port 5432 is already in use."
        in_use=true
    fi

    # Check ports 7073 and 7072 if reverse proxy is not enabled
    if [[ $reverse_proxy == "N" ]]; then
        if lsof -Pi :7073 -sTCP:LISTEN -t >/dev/null; then
            echo "Port 7073 is already in use."
            in_use=true
        fi

        if lsof -Pi :7072 -sTCP:LISTEN -t >/dev/null; then
            echo "Port 7072 is already in use."
            in_use=true
        fi
    fi

    if [[ $in_use == true ]]; then
        exit 1
    fi
}

# Function to clone repository and remove .git folder
clone_and_clean_repo() {
    local repo_url="$1"
    local folder="$2"
    
    git clone "$repo_url" "$folder"
    rm -rf "$folder/.git"
}

# Function to replace port mapping in docker-compose.yml if reverse proxy is enabled
replace_port_mapping() {
    local file_path="$1"
    
    sed -i 's/- 7073:7073/- 127.0.0.1:7073:7073/' "$file_path"
    sed -i 's/- 7072:7072/- 127.0.0.1:7072:7072/' "$file_path"
}

# Function to replace the services section in docker-compose.yml
replace_services() {
    local file="$1"
    local services_data="\n  caddy:\n    container_name: caddy-$reverse_proxy_url\n    image: caddy:alpine\n    restart: always\n    volumes:\n      - ./Caddyfile:/etc/caddy/Caddyfile\n      - ./certs:/certs\n      - ./config:/config\n      - ./data:/data\n      - ./sites:/srv\n    network_mode: \"host\""
    sed -i '/services:/c\services:'"$services_data"'' "$file"
}

# Function to replace URL in Caddyfile if it exists
replace_reverse_proxy_url() {
    local project_folder="$1"
    local reverse_proxy_url="$2"
    
    sed -i "s|https://example.com|https://$reverse_proxy_url|g" "$project_folder/Caddyfile"
}

# Function to check if a port is in use
check_port_in_use() {
    local port="$1"
    if lsof -Pi :"$port" -sTCP:LISTEN -t >/dev/null; then
        echo "Port $port is already in use."
        return 1
    else
        echo "Port $port is available."
        return 0
    fi
}

# Function to check whether git clone was successful
check_git_clone() {
    local folder="$1"
    if [ ! -d "$folder" ]; then
        echo "Error: Git clone failed or repository does not exist."
        exit 1
    fi
}

# Function to check whether docker-compose.yml file exists
check_docker_compose_file() {
    local folder="$1"

    if [ ! -f "$folder/docker-compose.yml" ]; then
        echo "Error: docker-compose.yml file does not exist in the project folder."
        exit 1
    fi
}

# Function to navigate to project folder and execute docker-compose up -d command
execute_docker_compose() {
    local folder="$1"
    local project_name="$2"

    cd "$folder" || exit

    # sed -i "s/container_name: flectra-psql/container_name: flectra-${project_name}-psql/g" docker-compose.yml
    sed -i "s/container_name: flectra3.0/container_name: flectra-${project_name}/g" docker-compose.yml

    docker-compose up -d
}

# Main script
if [[ $# -eq 0 ]]; then
    prompt_parameters
else
    # Parse command-line options
    while [[ $# -gt 0 ]]; do
        key="$1"
        case $key in
            -r|--reverse-proxy)
                reverse_proxy="$2"
                shift 2
                ;;
            -p|--proxy-url)
                reverse_proxy_url="$2"
                shift 2
                ;;
            -l|--project-location)
                project_location="$2"
                shift 2
                ;;
            *)
                echo "Unknown option: $1"
                print_usage
                exit 1
                ;;
        esac
    done
fi

# Check ports 5432 and 7073
check_ports

# Check if Docker is installed
if ! check_docker_installed; then
    echo "Installing Docker..."
    # Determine OS distribution
    os=$(awk -F= '/^NAME/{print $2}' /etc/os-release | tr -d '"')
    case "$os" in
        Debian*|Ubuntu*)
            install_docker_debian
            ;;
        CentOS*|Red*)
            install_docker_centos
            ;;
        *)
            echo "Unsupported OS: $os. Please install Docker manually."
            exit 1
            ;;
    esac
fi

# Check and install Docker Compose
if ! check_docker_compose_installed; then
    echo "Installing Docker Compose..."
    install_docker_compose
fi

# Check project location
check_project_location

# If project name is not provided, generate a project name based on reverse proxy URL
if [[ -z $project_name ]]; then
    if [[ $reverse_proxy == "Y" ]]; then
        project_name=$(slugify "$reverse_proxy_url")
    else
        project_name=$(generate_witty_project_name)
    fi
fi


# Create the project folder with lowercase name
project_name_lower=$(echo "$project_name" | tr '[:upper:]' '[:lower:]')
project_folder="$project_location/$project_name_lower"
mkdir -p "$project_folder"

# Clone repository and remove .git folder
echo "Cloning repository into project folder..."
clone_and_clean_repo "https://gitlab.com/flectra-hq/all-in-one-docker-compose-deploy" "$project_folder"

# Check whether git clone was successful
check_git_clone "$project_folder"

# Check whether docker-compose.yml file exists
check_docker_compose_file "$project_folder"

# Check whether git clone was successful
if [ ! -d "$project_folder" ]; then
    echo "Error: Git clone failed or repository does not exist."
    exit 1
fi

# Check whether docker-compose.yml file exists
if [ ! -f "$project_folder/docker-compose.yml" ]; then
    echo "Error: docker-compose.yml file does not exist in the project folder."
    exit 1
fi

# Replace port mapping in docker-compose.yml if reverse proxy is enabled
if [[ $reverse_proxy == "Y" ]]; then
    echo "Replacing port mapping in docker-compose.yml..."
    replace_port_mapping "$project_folder/docker-compose.yml"
    echo "Replacing services section in docker-compose.yml..."
    replace_services "$project_folder/docker-compose.yml"
fi

# Replace URL in Caddyfile if it exists
if [ -f "$project_folder/Caddyfile" ]; then
    echo "Replacing URL in Caddyfile..."
    replace_reverse_proxy_url "$project_folder" "$reverse_proxy_url"
fi


# Execute docker-compose up -d command in the project folder
echo "Executing docker-compose up -d command..."
execute_docker_compose "$project_folder" "$project_name"

# Print the parameters
echo "Reverse Proxy: $reverse_proxy"
echo "Reverse Proxy URL: https://$reverse_proxy_url"
echo "Project Location: $project_location"
echo "Project Name: $project_name"
echo "Project Folder: $project_folder"
